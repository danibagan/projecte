package Buscaminas;

import java.util.Hashtable;
import java.util.Scanner;

public class Jugador {
	
	static Scanner src = new Scanner(System.in);
	static Hashtable<String, Integer> jugadors = new Hashtable <String, Integer>();

	public static String definirJugador() {
		// TODO Auto-generated method stub
		
		String usuari;
		System.out.println("Digues el teu nom d'usuari: ");
		usuari = src.nextLine();
		
		if (!jugadors.containsKey(usuari)) {
			
			jugadors.put(usuari, 0);
			
		}
		
		System.out.println("El jugador " + usuari + " s' ha registrat correctament!");
		return usuari;
	}

	public static void actualitzarResultat(String jugador1, boolean resultat) {
		// TODO Auto-generated method stub
		
		if (jugadors.containsKey(jugador1) && resultat == true) {
			
			jugadors.put(jugador1, jugadors.get(jugador1) + 1);
			
		}
		
	}

	public static void veureResultat(String jugador) {
		// TODO Auto-generated method stub
			
			System.out.println("El jugador " + jugador + " te ni mes ni menys que " + jugadors.get(jugador) + " victories!!");
			
		}
		
	}
	

		
	



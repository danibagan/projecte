package Buscaminas;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * Clase que implementa el funcionamiento principal del juego de Buscaminas.
 * El método jugarPartida recibe el nombre del jugador y la dificultad del juego.
 */

public class Joc {

	static Random rnd = new Random();
	static Scanner src = new Scanner(System.in);
	
	 /**
     * Método principal para jugar una partida de Buscaminas.
     *
     * @param jugador1   Nombre del jugador.
     * @param dificultat Lista que representa la dificultad del juego [filas, columnas, minas].
     * @return true si el jugador gana, false si explota una bomba.
     */
	
	public static boolean jugarPartida(String jugador1, ArrayList<Integer> dificultat) {
		
		int [] casella = new int [2];
		
		int clicks = 0;

		boolean guanyador = false;

		boolean acabat = false;

		boolean bomba = false;

		int casellesDestRes = 0;

		int files = dificultat.get(0) + 2; // Obtiene el primer valor de la lista que son filas y lo guarda en una variable
		
        int columnes = dificultat.get(1) + 2; // Obtiene el segundo valor de la lista que son columnas y lo guarda en una variable
        
        int mines = dificultat.get(2); // Obtiene el tercer valor de la lista que son minas y lo guarda en una variable

		char[][] taulerJoc = new char[files][columnes]; // Creem els taulers buits depenent de la dificultat
																
		 // Crea el tablero resuelto con las minas y los números.
		char[][] tauleResolt = new char[files][columnes];

		crearTaulerJoc(taulerJoc, files, columnes);

		casella = crearTaulerResolt(tauleResolt, files, columnes, mines, casella, taulerJoc);
		 
		int x = casella[0];
		int y = casella[1];
		
		 taulerJoc[x][y] = tauleResolt[x][y];
		 
		// Inici partida

		do {

			ensenyarTauler(taulerJoc, files, columnes, tauleResolt, clicks); // Mostra l' estat actual del tauler
			
			casella = triarCasella(taulerJoc, files, columnes, tauleResolt, mines); // Tria la casella que vulguis per veure la seva funció
			
			clicks++;
			
			bomba = casellaSeleccionada(taulerJoc, tauleResolt, casella, bomba, casellesDestRes); // Desvela la función de la casilla
																																																				// funció
			acabat = casellesRestants(taulerJoc, tauleResolt, mines, casellesDestRes, files, columnes, acabat);  // ¿Quedan casillas para seguir jugando?
																																																	// si, no
			guanyador = comprovarGuanyador(guanyador, bomba, jugador1, acabat); // ¿No quedan bombas? Has ganado

		} while (acabat == false && bomba == false);

		if(guanyador == true){
			System.out.println("FELICITATS, HAS GUANYAT!");
		}
		else {
			System.out.println("QUE PENA, HAS EXPLOTAT");
		}
		
		return guanyador;
		

	}

	 /**
     * Comprueba si el jugador ganó la partida.
     *
     * @param guanyador Valor actual del ganador.
     * @param bomba     Indica si se encontró una bomba.
     * @param jugador1  Nombre del jugador.
     * @param acabat    Indica si la partida ha terminado.
     * @return true si el jugador ganó, false en caso contrario.
     */
	
	private static boolean comprovarGuanyador(boolean guanyador, boolean bomba, String jugador1, boolean acabat) {
		
		if (acabat == true && bomba == false) {
			
			guanyador = true;
		}
		
		return guanyador;
			
		
	}
	
	/**
     * Comprueba si todas las casillas han sido destapadas y el juego ha terminado.
     *
     * @param taulerJoc       Tablero de juego.
     * @param tauleResolt     Tablero resuelto.
     * @param mines           Número de minas.
     * @param casellesDestRes Número de casillas destapadas.
     * @param files           Número de filas.
     * @param columnes        Número de columnas.
     * @param acabat          Indica si la partida ha terminado.
     * @return true si todas las casillas han sido destapadas, false en caso contrario.
     */
	
	private static boolean casellesRestants(char[][] taulerJoc, char[][] tauleResolt, int mines, int casellesDestRes, int files, int columnes, boolean acabat) {
		// TODO Auto-generated method stub
		
		if(casellesDestRes == (files * columnes - mines)) {
			
			acabat = true;
			
		}
		else {
			
			acabat = false;
			
		}
		return acabat;
	}
	
	 /**
     * Realiza la acción correspondiente a la casilla seleccionada.
     *
     * @param taulerJoc       Tablero de juego.
     * @param tauleResolt     Tablero resuelto.
     * @param casella         Coordenadas de la casilla seleccionada.
     * @param bomba           Indica si se encontró una bomba.
     * @param casellesDestRes Número de casillas destapadas.
     * @return true si se encontró una bomba, false en caso contrario.
     */
	
	private static boolean casellaSeleccionada(char[][] taulerJoc, char[][] tauleResolt, int[] casella, boolean bomba, int casellesDestRes) {
		// TODO Auto-generated method stub
		
		int x = casella[0];
		int y = casella[1];
		
		if(tauleResolt[x][y] == '■') {
			
			bomba = true;
			
		}
		else if (tauleResolt[x][y] != '□') {
			taulerJoc[x][y] = tauleResolt[x][y];
			casellesDestRes++;
		}
		else {
			taulerJoc = casellaBlanca(casella, tauleResolt, taulerJoc, casellesDestRes);
		}
			
		return bomba;
	}
	
	
	 /**
     * Realiza la acción correspondiente a una casilla blanca seleccionada.
     *
     * @param casella         Coordenadas de la casilla blanca.
     * @param tauleResolt     Tablero resuelto.
     * @param taulerJoc       Tablero de juego.
     * @param casellesDestRes Número de casillas destapadas.
     * @return Tablero actualizado después de destapar las casillas blancas.
     */
	
	private static char[][] casellaBlanca(int [] casella, char[][] tauleResolt, char[][] taulerJoc, int casellesDestRes) {
		// TODO Auto-generated method stub
		
	    int x = casella[0];
	    int y = casella[1];

	    if (tauleResolt[x][y] == '□') {
	    	
	        if (taulerJoc [x][y] != '*') {
	        	
	            taulerJoc [x][y] = '*';
	            
	            casellesDestRes++;

	            taulerJoc = casellaBlanca(new int[]{x - 1, y - 1}, tauleResolt, taulerJoc, casellesDestRes);
	            taulerJoc = casellaBlanca(new int[]{x - 1, y}, tauleResolt, taulerJoc,  casellesDestRes);
	            taulerJoc = casellaBlanca(new int[]{x - 1, y + 1}, tauleResolt, taulerJoc,  casellesDestRes);
	            taulerJoc = casellaBlanca(new int[]{x, y- 1}, tauleResolt, taulerJoc,  casellesDestRes);
	            taulerJoc = casellaBlanca(new int[]{x, y+ 1}, tauleResolt, taulerJoc,  casellesDestRes);
	            taulerJoc = casellaBlanca(new int[]{x + 1, y - 1}, tauleResolt, taulerJoc,  casellesDestRes);
	            taulerJoc = casellaBlanca(new int[]{x + 1, y}, tauleResolt, taulerJoc,  casellesDestRes);
	            taulerJoc = casellaBlanca(new int[]{x + 1, y + 1}, tauleResolt, taulerJoc,  casellesDestRes);
	        }
	    }
	    
	    else {
	    	
	        taulerJoc[x][y] = tauleResolt[x][y];
	    }

	    return taulerJoc;
	}

	  /**
     * Lee las coordenadas de la casilla que el jugador quiere seleccionar.
     *
     * @param taulerJoc   Tablero de juego.
     * @param files       Número de filas.
     * @param columnes    Número de columnas.
     * @param tauleResolt Tablero resuelto.
     * @param mines       Número de minas.
     * @return Coordenadas de la casilla seleccionada.
     */
	
	private static int[] triarCasella(char[][] taulerJoc, int files, int columnes, char[][] tauleResolt, int clicks) {
		// TODO Auto-generated method stub
		
		int [] casella = new int [2];
		
		int x = 0;
		int y = 0;
		

		
		do {
			
			System.out.println("Que casella vols marcar?");
		
			x = Dificultat.llegirNum("fila: ", "ERROR, introdueix un numero vàlid");
			
			System.out.println("");
			
			y = Dificultat.llegirNum("columna: ", "ERROR, introdueix un numero vàlid");
			
		}while(x < 1 || x > files - 2 || y < 1 || y > columnes - 2);
		
			
		casella[0] = x;
		casella[1] = y;
		
		return casella;

	}
		
	/**
     * Muestra el estado actual del tablero.
     *
     * @param taulerJoc   Tablero de juego.
     * @param columnes    Número de columnas.
     * @param files       Número de filas.
     * @param tauleResolt Tablero resuelto.
     * @param clicks      Número de clics realizados.
     */
	
	private static void ensenyarTauler(char[][] taulerJoc, int columnes, int files, char[][] tauleResolt, int clicks) {
		// TODO Auto-generated method stub
		System.out.println();
		System.out.println("-------------------BUSCAMINES-------------------");
		System.out.println();
		System.out.println("Clicks: " + clicks);
		System.out.println();
		
		for (int i = 1; i < files - 1; i++) {
			
			for (int j = 1; j < columnes - 1; j++) {
				
				System.out.print(taulerJoc[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
		
		for (int i = 1; i < files - 1; i++) {
			
			for (int j = 1; j < columnes - 1; j++) {
				
				System.out.print(tauleResolt[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println("-------------------------------------------------");
	}
	
	 /**
     * Crea el tablero resuelto con las minas y los números.
     *
     * @param tauleResolt Tablero resuelto.
     * @param files       Número de filas.
     * @param columnes    Número de columnas.
     * @param mines       Número de minas.
     * @param casella     Coordenadas de la casilla seleccionada.
     * @param taulerJoc   Tablero de juego.
     * @return Tablero resuelto actualizado.
     */	
	
	private static int[] crearTaulerResolt(char[][] tauleResolt, int files, int columnes, int mines, int[] casella, char[][] taulerJoc) {
		// TODO Auto-generated method stub
		
		casella = triarCasella(taulerJoc, files, columnes, tauleResolt, mines);	
		
		for (int i = 0; i < files; i++) {
			for (int j = 0; j < columnes; j++) {
				if (i == 0 || j == 0 || i == files - 1|| j == columnes - 1) {
					tauleResolt[i][j] = ' ';
				} else {
					tauleResolt[i][j] = '□';
				}
			}
		}
		
		tauleResolt = intMines(tauleResolt, mines, files, columnes, casella);
		tauleResolt = intNums(tauleResolt, files, columnes);
	
		return casella;
	}


    /**
     * Coloca los números en las casillas adyacentes a las minas.
     *
     * @param tauleResolt Tablero resuelto.
     * @param files       Número de filas.
     * @param columnes    Número de columnas.
     * @return Tablero resuelto actualizado con los números.
     */
	
	private static char[][] intNums(char[][] tauleResolt, int files, int columnes) {
		// TODO Auto-generated method stub

		for (int i = 1; i < files - 1; i++) {
			for (int j = 1; j < columnes - 1; j++) {
				
				int minesproperes = 0;
				
				if (tauleResolt[i][j] == '■') {
					
				}

				else {
					if (tauleResolt[i][j + 1] == '■') {
						minesproperes++;
					}
					if (tauleResolt[i][j - 1] == '■') {
						minesproperes++;
					}
					if (tauleResolt[i + 1][j] == '■') {
						minesproperes++;
					}
					if (tauleResolt[i - 1][j] == '■') {
						minesproperes++;
					}
					if (tauleResolt[i + 1][j + 1] == '■') {
						minesproperes++;
					}
					if (tauleResolt[i - 1][j - 1] == '■') {
						minesproperes++;
					}
					if (tauleResolt[i - 1][j + 1] == '■') {
						minesproperes++;
					}
					if (tauleResolt[i + 1][j - 1] == '■') {
						minesproperes++;
					}
					if(minesproperes > 0) {
						
						tauleResolt[i][j] = Character.forDigit(minesproperes,10);
						
					}
				}
				
				
			
			}
		}

		return tauleResolt;
	}
	
	 /**
     * Coloca las minas en el tablero resuelto.
     *
     * @param tauleResolt Tablero resuelto.
     * @param mines       Número de minas.
     * @param files       Número de filas.
     * @param columnes    Número de columnas.
     * @param casella     Coordenadas de la casilla seleccionada.
     * @return Tablero resuelto actualizado con las minas.
     */
	
	private static char[][] intMines(char[][] tauleResolt, int mines, int files, int columnes, int[] casella) {
		// TODO Auto-generated method stub

		int x = 0;
		int y = 0;

		while (mines > 0) {
			
			do {

				x = rnd.nextInt(files);
				y = rnd.nextInt(columnes);

			} while (tauleResolt[x][y] != '□' || (x == casella[0] && y == casella[1]));
			
			tauleResolt[x][y] = '■';
			
			mines--;
		}

		return tauleResolt;
	}

	 /**
     * Crea el tablero de juego.
     *
     * @param taulerJoc Tablero de juego.
     * @param files     Número de filas.
     * @param columnes  Número de columnas.
     */
	
	private static void crearTaulerJoc(char[][] taulerJoc, int files, int columnes) {
		// TODO Auto-generated method stub

		for (int i = 1; i < files - 1; i++) {
			for (int j = 1; j < columnes - 1; j++) {
				
				if (i == 0 || j == 0 || i == files - 1|| j == columnes - 1) {
					
					taulerJoc[i][j] = ' ';
					
				} else {
					
					taulerJoc[i][j] = '□';
					
				}
			}
		}
	}

}

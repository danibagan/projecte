package Buscaminas;

import java.util.ArrayList;

/**
 * Clase que maneja la elección de la dificultad del juego.
 */

import java.util.Scanner;

public class Dificultat {

	static Scanner src = new Scanner(System.in);
	
	   /**
     * Método que permite al jugador elegir la dificultad del juego.
     *
     * @param jugador El nombre del jugador.
     * @return ArrayList que contiene la dificultad elegida (número de filas, columnas y minas).
     */
	
	public static ArrayList<Integer> triarMode(String jugador) {
		// TODO Auto-generated method stub
		
		ArrayList<Integer> dificultat = new ArrayList<Integer>();

		
		int files = 0;
		int columnes = 0;
		int mines = 0;
		
		
		System.out.println("Selecciona la mesura del tauler que mes et vingui de gust: ");
		
		System.out.println("1- Nivell principiant: 8 x 8 caselles i 10 mines");
		
		System.out.println("2- Nivell intermitg: 16 x 16 caselles i 40 mines");
		
		System.out.println("3- Nivell expert: 16 x 30 caselles i 99 mines");
		
		System.out.println("4- Nivell personalitzat: personalitza el tauler escollint el numero de caselles i mines");
		
		System.out.println("0- EXIT");
		
		System.out.println("Tria una opció: ");
		
		int op = src.nextInt();
		
		System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		
		switch (op) {
		
		case 1: 
			
			System.out.println("Has triat la dificultat principiant");
			
			files = 8;
			columnes = 8;
			mines = 10;
			
			dificultat.add(files);
			dificultat.add(columnes);
			dificultat.add(mines);
			
		break;
		
		case 2: 
			
			System.out.println("Has triat la dificultat intermitga");
			
			files = 16;
			columnes = 16;
			mines = 40;
			
			dificultat.add(files);
			dificultat.add(columnes);
			dificultat.add(mines);
			
		break;
		
		case 3: 
			
			System.out.println("Has triat la dificultat experta");
			
			files = 16;
			columnes = 30;
			mines = 99;
			
			dificultat.add(files);
			dificultat.add(columnes);
			dificultat.add(mines);
			
        break;
		
		case 4: 
			
			System.out.println("Has triat la dificultat personalitzada");
			System.out.println();
			
			do {
			files = llegirNum("Tria un numero de files entre 8 i 24: ", "Error, instrodueix un valor vàlid");
			System.out.println();
			
			}while(files > 24 || files < 8);
			
			do {
			columnes = llegirNum("Tria un numero de columnes entre 8 i 32: ", "Error, instrodueix un valor vàlid");
			System.out.println();
			
			}while(columnes > 32 || columnes < 8);
			
			
			do {
			mines = llegirNum("Tria un numero de mines adecuat: ", "Error, instrodueix un valor vàlid");
			System.out.println();
				
			}while ((files * columnes)/3 < mines);
				
			dificultat.add(files);
			dificultat.add(columnes);
			dificultat.add(mines);
			
		break;
		
		
		case 0: 
			
			
		break;
		
		default: 
			
			System.out.println("Error, introdueix un valor correcte");
			System.out.println();
		}
		return dificultat;	
	}

	/**
     * Método que lee un número del usuario y asegura que sea un valor válido.
     *
     * @param missatge1 Mensaje que indica al usuario qué se espera que ingrese.
     * @param missatge2 Mensaje de error que se muestra si se ingresa un valor no válido.
     * @return El número ingresado por el usuario.
     */
	
	static int llegirNum(String missatge1, String missatge2) {
		// TODO Auto-generated method stub
		boolean correcte = false;
		
		int opcio = 0;
		
		do {
			System.out.println(missatge1);

			try {
				
				opcio = src.nextInt();
				
				correcte = true;
			}
			
			catch (Exception e){
				
				System.out.println(missatge2);
				
				src.nextLine();
			}
		}
		
		while (correcte == false);
		
		return opcio;
	}
		
	
}


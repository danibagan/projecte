package Buscaminas;

import java.util.ArrayList;
import java.util.Scanner;

public class Programa {

	/** 1.- Mostrar Ajuda
	2.- Definir Jugadors
	3.- Jugar Partida
	4.- Veure Jugadors
	5.- Triar dificultat
	0.- Sortir			**/
static Scanner src = new Scanner(System.in);

/**
 * Método principal que ejecuta el programa del juego Buscaminas.
 *
 * @param args Los argumentos de la línea de comandos (no se utilizan en este caso).
 */

public static void main(String[] args) {
	int opcio = 0; // Desa l'opció escollida per l'usuari
	
	String jugador = "";
	boolean resultat = false;
	ArrayList<Integer> dificultat = new ArrayList<Integer>(); // creem la llista on es guarden les files columnes i mines depenent de la dificultat que triem
	dificultat.add(8);
	dificultat.add(8);
	dificultat.add(10);
	
	
	boolean definit = false;
	
	boolean mode = false;
	
	do { 
		
		opcio = mostrarMenu();
		
			switch (opcio) {
			
			case 1: 
				
				Ajuda.mostrarAjuda();
				
			break;
			
			case 2: 
				
				jugador = Jugador.definirJugador();
				definit = true;
				
			break;
			
			case 3: 
				
				if (definit && mode) {
					
                    resultat = Joc.jugarPartida(jugador,dificultat);
                    Jugador.actualitzarResultat(jugador, resultat);
                    definit = false;
                    
                } else {
                    System.out.println("Error, has de definir els jugadors i definir la dificultat abans de començar.");
                }
				
            break;
			
			case 4: 
				
				Jugador.veureResultat(jugador);
				
			break;
			
			case 5:
				
				dificultat.clear();
				
				if (definit) {
				
				dificultat = Dificultat.triarMode(jugador);
				mode = true;
				
				
				}else {
                    System.out.println("Error, has de definir els jugadors abans de començar.");
                }
			
			case 0: 
				
		
				
			break;
			
			default: 
				
				System.out.println("Error, introdueix un valor correcte");
				System.out.println();
		}
	}
	
	while (opcio != 0);
	
}

/**
 * Método que muestra el menú del juego y lee la opción del usuario.
 *
 * @return La opción elegida por el usuario.
 */

private static int mostrarMenu() {
	
	System.out.println("--------------------BUSCAMINES----------------------");
	
	System.out.println();
	
	System.out.println("1- Mostrar ajuda");
	
	System.out.println("2- Definir jugadors");
	
	System.out.println("3- Jugar partida");
	
	System.out.println("4- Resultats jugador");
	
	System.out.println("5- Seleccionar dificultat");
	
	System.out.println("0- Sortir");
	
	System.out.println();
	
	System.out.println("----------------------------------------------------");
	
	System.out.println();
	
	boolean correcte = false;
	
	int opcio = 0;
	
	do {
		System.out.println("Escull una opció: ");

		try {
			
			opcio = src.nextInt();
			
			correcte = true;
		}
		
		catch (Exception e){
			
			System.out.println("Error, opció invàlida ");
			
			src.nextLine();
		}
	}
	
	while (correcte == false);
	
	return opcio;
}

}

	


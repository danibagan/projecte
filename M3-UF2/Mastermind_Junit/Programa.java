package Mastermind_Junit;

import java.util.Scanner;

/**
 * Esta clase representa el programa principal para el juego Mastermind.
 * Proporciona un menú para que los usuarios generen una combinación, jueguen una partida o salgan.
 */
public class Programa {

    /** 
     * Punto de entrada del programa.
     * @param args Argumentos de la línea de comandos (no se utilizan en este programa).
     */
	static Scanner src = new Scanner(System.in);
    public static void main(String[] args) {
        int opcio = 0; // Almacena la opción elegida por el usuario

        int[] combinacio = new int[4];

        boolean comb = false;

        boolean guanyador = false;

        do {

            opcio = mostrarMenu();

            switch (opcio) {

                case 1:

                    combinacio = Generacio.generarCombinacio(combinacio);

                    comb = true;

                    break;

                case 2:

                    if (comb == true) {

                        guanyador = Joc.jugarPartida(combinacio, guanyador);
                    }

                    else {

                        System.out.println("Debes generar una combinación antes de poder jugar una partida.");
                    }

                    break;

                case 0:

                    System.out.println();
                    System.out.println("Fin del Juego");

                    break;

                default:

                    System.out.println("Error, introduce un valor correcto");
                    System.out.println();
            }
        }

        while (opcio != 0);

    }

    /**
     * Muestra el menú principal del juego Mastermind.
     * @return La opción elegida por el usuario.
     */
    private static int mostrarMenu() {
        System.out.println("-----------MASTERMIND-----------");

        System.out.println();

        System.out.println("1- Generar Combinación");

        System.out.println("2- Jugar Partida");

        System.out.println("0- Salir");

        System.out.println();

        boolean correcto = false;

        int opcion = 0;

        do {
            System.out.println("Elige una opción: ");

            try {

                opcion = src.nextInt();

                correcto = true;
            }

            catch (Exception e){

                System.out.println("Error, opción inválida ");

                src.nextLine();
            }
        }

        while (correcto == false);

        return opcion;
    }
}

package Mastermind_Junit;

import static org.junit.Assert.*;

import org.junit.Test;

public class JocTest {

	 @Test
	    public void testComprovarNumeros() {
	      
	        assertTrue(Joc.comprovarString("1234"));
	        assertTrue(Joc.comprovarString("1423"));
	        assertTrue(Joc.comprovarString("4982"));
	        assertTrue(Joc.comprovarString("8888"));	            
	        assertFalse(Joc.comprovarString("123"));
	        assertFalse(Joc.comprovarString("12345"));	        	      
	        assertFalse(Joc.comprovarString("qwer"));
	        assertFalse(Joc.comprovarString("a234"));
	        assertFalse(Joc.comprovarString("1 3 43"));
	        assertFalse(Joc.comprovarString("X2X4"));
	    }

	 @Test
	    public void testComprovarCombinacio() {
	        
	        assertTrue(Joc.comprovarCombi("1234"));
	        assertTrue(Joc.comprovarCombi("5678"));
	        assertFalse(Joc.comprovarCombi("1223")); 
	        assertFalse(Joc.comprovarCombi("1123")); 
	        assertFalse(Joc.comprovarCombi("1122")); 
	        assertFalse(Joc.comprovarCombi("5252")); 
	        assertFalse(Joc.comprovarCombi("1114")); 
	        assertFalse(Joc.comprovarCombi("1811"));
	        assertFalse(Joc.comprovarCombi("1111")); 
	        assertFalse(Joc.comprovarCombi("2111"));
	    }
	}

// hacer que la entrada sea un string seguido y entonces hacer un metodo que sea pasar el string a integer i ahi hacer junit y luego otro metodo para confirmar si esta bien

package Mastermind_Junit;

import java.util.Scanner;

/**
 * Esta clase implementa la lógica para un juego de Mastermind. Incluye métodos para jugar una ronda,
 * verificar si hay un ganador, analizar respuestas y otras funciones de utilidad necesarias para el juego.
 */

public class Joc {
	
	static Scanner src = new Scanner(System.in);

	   /**
     * Ejecuta una sola ronda del juego Mastermind.
     *
     * @param combinacio Un array que representa la combinación correcta.
     * @param guanyador Una bandera booleana que indica si el jugador ha ganado.
     * @return Un booleano que indica si el jugador ha ganado el juego.
     */
	
	public static boolean jugarPartida(int[] combinacio, boolean guanyador) {
		// TODO Auto-generated method stu								
		char [] tauler = new char [4];
		
		int [] resposta = new int [4];
		
		
		do {
			
			resposta = preguntarNumero();
			
			tauler = analitzarResposta(combinacio, resposta);
			
			guanyador = comprovarGuanyador(combinacio, resposta, guanyador, tauler);
		
		}while (guanyador == false);
		
		if (guanyador == true) {
			
			System.out.println("FELICITATS, has guanyat!");
			System.out.println();
		}
		
	return guanyador;
	}

	 /**
     * Verifica si la respuesta del jugador coincide con la combinación ganadora.
     *
     * @param combinacio La combinación correcta.
     * @param resposta La suposición del jugador.
     * @param guanyador El estado actual del juego, si el jugador ha ganado o no.
     * @param tauler El tablero del juego representado como un array de caracteres.
     * @return Un booleano que indica si el jugador ha ganado.
     */
	
	private static boolean comprovarGuanyador(int[] combinacio, int[] resposta, boolean guanyador, char[] tauler) {
		// TODO Auto-generated method stub
		
		if (tauler[0] == 'O' && tauler[1] == 'O' && tauler[2] == 'O' && tauler[3] == 'O') {
			
			guanyador = true;
			
		}
		
		return guanyador;
		
	}

	 /**
     * Analiza la respuesta del jugador y actualiza el tablero del juego en consecuencia.
     *
     * @param combinacio La combinación correcta.
     * @param resposta La suposición del jugador.
     * @return El tablero del juego actualizado como un array de caracteres.
     */
	
	private static char[] analitzarResposta(int[] combinacio, int[] resposta) {
		// TODO Auto-generated method stub
		
		char[] tauler = {'_', '_', '_', '_'};
		
		tauler = adivinarNum(combinacio, resposta, tauler);;
		
		tauler = plenaNum(combinacio, resposta, tauler);
		
		
		System.out.println("Si el simbol es: 'O' es plena, si es 'X' falta adivinar la posicio");
		
		System.out.println(tauler[0] + " " + tauler[1] + " " + tauler[2] + " " + tauler[3] + " ");
		
		return tauler;
	}

	 /**
     * Actualiza el tablero del juego marcando las posiciones con los números correctos.
     *
     * @param combinacio La combinación correcta.
     * @param resposta La suposición del jugador.
     * @param tauler El tablero del juego representado como un array de caracteres.
     * @return El tablero del juego actualizado.
     */
	
	private static char[] plenaNum(int[] combinacio, int[] resposta, char[] tauler) {
		// TODO Auto-generated method stub
		
		if(combinacio[0] == resposta[0]) {
			
			tauler[0] = 'O';
			
		}
		if(combinacio[1] == resposta[1]) {
			
			tauler[1] = 'O';
			
		}
		if(combinacio[2] == resposta[2]) {
			
			tauler[2] = 'O';
			
		}
		if(combinacio[3] == resposta[3]) {
			
			tauler[3] = 'O';
			
		}
		
		
		return tauler;
	}

	/**
     * Actualiza el tablero del juego marcando las posiciones donde el jugador ha adivinado
     * el número correcto pero no la posición correcta.
     *
     * @param combinacio La combinación correcta.
     * @param resposta La suposición del jugador.
     * @param tauler El tablero del juego representado como un array de caracteres.
     * @return El tablero del juego actualizado.
     */
	
	private static char[] adivinarNum(int[] combinacio, int[] resposta, char[] tauler) {
		// TODO Auto-generated method stub
		
		if(combinacio[0] == resposta[1] || combinacio[0] == resposta[2] || combinacio[0] == resposta[3]) {
			
			tauler[0] = 'X';
			
			
		}
		if(combinacio[1] == resposta[0] || combinacio[1] == resposta[2] || combinacio[1] == resposta[3]) {
			
			tauler[1] = 'X';
			
		}
		if(combinacio[2] == resposta[0] || combinacio[2] == resposta[1] || combinacio[2] == resposta[3]) {
			
			tauler[2] = 'X';
			
		}
		if(combinacio[3] == resposta[0] || combinacio[3] == resposta[2] || combinacio[3] == resposta[1]) {
			
			tauler[3] = 'X';
			
		}
		
		return tauler;
	}

	  /**
     * Solicita al jugador una suposición y devuelve un array de enteros representando
     * dicha suposición.
     *
     * @return La suposición del jugador como un array de enteros.
     */
	private static int[] preguntarNumero() {
		// TODO Auto-generated method stub

		
		boolean correcte = false;
	
		String combi = "";
		
		int [] resposta = new int [4];
		
		while(!correcte) {
			
			System.out.println("Introdueix quatre numeros d'un digit sense repeticions: ");
			
			combi = src.nextLine();
			
			System.out.println();
			correcte = comprovarString(combi);
			
		
		if (correcte) {
			
			correcte = comprovarCombi(combi);
			
		}
		}
		
		for(int i = 0; i < 4; i++) {
			
			resposta[i] = combi.charAt(i) - '0';
		}
		
		
		return resposta;
		
	}

	  /**
     * Comprueba si la suposición ingresada por el jugador contiene números repetidos.
     *
     * @param combi La suposición del jugador como una cadena de caracteres.
     * @return True si no hay números repetidos, False si hay números repetidos.
     */
	
	static boolean comprovarCombi(String combi) {
		// TODO Auto-generated method stub
		boolean correcte = true;
    	
    	if ((combi.charAt(0) == combi.charAt(1)) || (combi.charAt(0) == combi.charAt(2)) || (combi.charAt(0) == combi.charAt(3)) || (combi.charAt(1) == combi.charAt(2)) || (combi.charAt(1) == combi.charAt(3)) || (combi.charAt(2) == combi.charAt(3))) {
    		correcte = false;
    		System.out.println("ERROR. No es poden repetir els numeros");
    	}
    	
    	return correcte;
	}

	 /**
     * Comprueba si la suposición ingresada por el jugador es válida.
     *
     * @param combi La suposición del jugador como una cadena de caracteres.
     * @return True si la suposición es válida, False si no lo es.
     */

	static boolean comprovarString(String combi) {
		// TODO Auto-generated method stub
		
		int[] resposta = new int [4];
		
		boolean correcte = true;
		
		if(combi.length() < 4) {
			correcte = false;
			System.out.println("Et falten numeros per introduir");
		}
		
		else if(combi.length() > 4) {
			correcte = false;
			System.out.println("Et sobren numeros introduits");
		}
		
		else {
			
			for(int i = 0; i < 4; i++) {
				
				if(combi.charAt(i) < '0' || combi.charAt(i) > '9') {
					
					correcte = false;
					
					System.out.println("Has d'introduir numeros mes entre 0 i 9 (inclosos)");
					
					resposta = preguntarNumero();
					
				}
				
			}
			
		}
		
		return correcte;
		
	}
}
	

	
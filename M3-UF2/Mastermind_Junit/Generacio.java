package Mastermind_Junit;

import java.util.Random;

/**
 * Esta clase proporciona métodos para generar combinaciones aleatorias en el juego Mastermind.
 */

public class Generacio {
	
	static Random rnd = new Random();

	/**
     * Genera una combinación aleatoria de números del 0 al 9 sin repetición.
     *
     * @param combinacio El array que almacenará la combinación generada.
     * @return El array con la combinación aleatoria generada.
     */
	
	public static int[] generarCombinacio(int[] combinacio) {
		// TODO Auto-generated method stub
		
		
		combinacio[0] = rnd.nextInt(9);
	
		do {
			
		combinacio[1] = rnd.nextInt(9);
		
		}while(combinacio[1] == combinacio[0]);
		
		do {
			
			combinacio[2] = rnd.nextInt(9);
			
		}while(combinacio[2] == combinacio[0] || combinacio[2] == combinacio[1]);
		
		do {
			combinacio[3] = rnd.nextInt(9);
			
		}while(combinacio[3] == combinacio[0] || combinacio[3] == combinacio[1] || combinacio[3] == combinacio[2]);
		
		return combinacio;
	}

}

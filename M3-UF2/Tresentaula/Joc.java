package Tresentaula;

import java.util.Scanner;

public class Joc {

	public static int jugarPartida(String jugador1, String jugador2) {
		// TODO Auto-generated method stub
		
		boolean acabat = false;
		
		int guanyador = 0;									
		
		char[][] tauler = new char[3][3];					
		
		for (int i = 0; i < 3; i++) {
			
			for (int j = 0; j < 3; j++) {
				
	            tauler[i][j] = '_';
	        }
        }
		
		do {
			
			if (!casellesDisponibles(tauler) && guanyador == 0) {
	            System.out.println("Os heu quedat sense caselles, per tant heu empatat.");
	            acabat = true;
	        }
			
			mostrarTauler(tauler, jugador1, jugador2);
			
			if (!acabat) {
				System.out.print(jugador1 + ", ");
	            tauler = demanarCasella(tauler, 'X'); 		
	            acabat = comprovarTauler(tauler);
	        }			
			
			if (!casellesDisponibles(tauler) && guanyador == 0) {
	            System.out.println("Os heu quedat sense caselles, per tant heu empatat.");
	            acabat = true;
	        }
			
			mostrarTauler(tauler, jugador1, jugador2);
			
			if (!acabat) {
				System.out.print(jugador2 + ", ");
	            tauler = demanarCasella(tauler, 'O');
	            acabat = comprovarTauler(tauler);
	        }
			
			guanyador = comprovarGuanyador(tauler, acabat);
		}
		
		while (acabat == false);
		
		if (guanyador == 1) {
			
			System.out.println("FELICITATS " + jugador1 + ", has gunayat!");
			System.out.println();
		}
		
		else if (guanyador == 2) {
			
			System.out.println("FELICITATS " + jugador2 + ", has gunayat!");
			System.out.println();
		}
		
		else {
			
			System.out.println("Dediqueu-vos a un altre cosa...");
		}
		
		return guanyador;
	}

	private static boolean casellesDisponibles(char[][] tauler) {
		// TODO Auto-generated method stub
		
		for (int i = 0; i < 3; i++) {
			
	        for (int j = 0; j < 3; j++) {
	        	
	            if (tauler[i][j] == '_') {
	                return true;
	                
	            }
	        }
	    }

		return false;
	}

	private static void mostrarTauler(char[][] tauler, String jugador1, String jugador2) {
		// TODO Auto-generated method stub
		System.out.println();
		System.out.println();
		System.out.print(jugador1 + " juga amb X ");
		System.out.println();
		System.out.print(jugador2 + " juga amb O ");
		
		System.out.println();
		System.out.println();
		
		System.out.println("TAULER: ");
		System.out.println();
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print(tauler[i][j] + " ");
	        }
				System.out.println();
        }
		
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
	}

	private static char[][] demanarCasella(char[][] tauler, char simbol) {
		// TODO Auto-generated method stub

		Scanner src = new Scanner(System.in);
		
		  int i = 0;
	        int j = 0;

	        do {
	        	
	            try {
	                System.out.print("Digues una fila i una columna: ");
	                
	                i = src.nextInt();
	                j = src.nextInt();

	                if (i < 0 || i >= 3 || j < 0 || j >= 3 || tauler[i][j] != '_') {
	                	
	                    System.out.println("Casella no disponible, inteta-ho de nou.");
	                    
	                }
	            } 
	            
	            catch (Exception e) {
	            	
	                System.out.println("Valor no vàlid, introdueix valors numèrics.");
	                
	                src.nextLine();
	            }

	        } while (i < 0 || i >= 3 || j < 0 || j >= 3 || tauler[i][j] != '_');

	        tauler[i][j] = simbol;

	        return tauler;
	    }

	

	private static boolean comprovarTauler(char[][] tauler) {
		// TODO Auto-generated method stub
		
		 boolean fi = false;

		 // files
		    for (int i = 0; i < 3; i++) {
		        if (tauler[i][0] != '_' && tauler[i][1] == tauler[i][0] && tauler[i][2] == tauler[i][0]) {
		        	fi = true;
		        }
		    }


		  // columnes
		    if (!fi) {
		        for (int j = 0; j < 3; j++) {
		            if (tauler[0][j] != '_' && tauler[1][j] == tauler[0][j] && tauler[2][j] == tauler[0][j]) {
		            	fi = true;
		            }
		        }
		    }

		    
		   // diagonals 
		    if (!fi && tauler[0][0] != '_' && tauler[0][0] == tauler[1][1] && tauler[0][0] == tauler[2][2]) {
		    	fi = true;
		    }
		    
		    if (!fi && tauler[0][2] != '_' && tauler[0][2] == tauler[1][1] && tauler[0][2] == tauler[2][0]) {
		    	fi = true;
		    }

		    return fi;
		}

	private static int comprovarGuanyador(char[][] tauler, boolean acabat) {
		// TODO Auto-generated method stub
		
		int guanyador = 0;
		
		boolean trobat = false;
		
		
		if (acabat == true) {
			
			//files
		    for (int i = 0; i < 3; i++) {
		        if (tauler[i][0] == 'X' && tauler[i][1] == 'X' && tauler[i][2] == 'X') {
		        	trobat = true;
		        	guanyador = 1;
		        }
		        
		        else if (tauler[i][0] == 'O' && tauler[i][1] == 'O' && tauler[i][2] == 'O') {
		        	trobat = true;
		        	guanyador = 2;
		        }
		    }

		    
		    //columnes
		    if (!trobat) {
		        for (int j = 0; j < 3; j++) {
		            if (tauler[0][j] == 'X' && tauler[1][j] == 'X' && tauler[2][j] == 'X') {
		            	trobat = true;
		            	guanyador = 1;
		            }
		            
		            else if (tauler[0][j] == 'O' && tauler[1][j] == 'O' && tauler[2][j] == 'O') {
		            	trobat = true;
		            	guanyador = 2;
		            }
		            
		        }
		    }



		    //diagonals
		    if (!trobat && tauler[0][0] == 'X' && tauler[1][1] == 'X' && tauler[2][2] == 'X') {
		    	trobat = true;
		    	guanyador = 1;
		    }
		    
		    else if (!trobat && tauler[0][0] == 'O' && tauler[1][1] == 'O' && tauler[2][2] == 'O') {
		    	trobat = true;
		    	guanyador = 2;
		    }
		    
		    if (!trobat && tauler[0][2] == 'X' && tauler[1][1] == 'X' && tauler[2][0] == 'X') {
		    	trobat = true;
		    	guanyador = 1;
		    }
		    
		    else if (!trobat && tauler[0][2] == 'O' && tauler[1][1] == 'O' && tauler[2][0] == 'O') {
		    	trobat = true;
		    	guanyador = 2;
		    }
			
		}
		
		else {
			System.out.println("Encara no heu acabat");
		}
		
		return guanyador;
	}



}

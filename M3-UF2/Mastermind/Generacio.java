package Mastermind;

import java.util.Random;

public class Generacio {
	
	static Random rnd = new Random();

	public static int[] generarCombinacio(int[] combinacio) {
		// TODO Auto-generated method stub
		
		
		combinacio[0] = rnd.nextInt(9);
	
		do {
			
		combinacio[1] = rnd.nextInt(9);
		
		}while(combinacio[1] == combinacio[0]);
		
		do {
			
			combinacio[2] = rnd.nextInt(9);
			
		}while(combinacio[2] == combinacio[0] || combinacio[2] == combinacio[1]);
		
		do {
			combinacio[3] = rnd.nextInt(9);
			
		}while(combinacio[3] == combinacio[0] || combinacio[3] == combinacio[1] || combinacio[3] == combinacio[2]);
		
		return combinacio;
	}

}

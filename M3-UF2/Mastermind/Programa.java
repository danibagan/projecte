package Mastermind;

import java.util.Scanner;

public class Programa {

	/** 1.- Mostrar Ajuda
	2.- Definir Jugadors
	3.- Jugar Partida
	4.- Veure Jugadors
	0.- Sortir			**/
static Scanner src = new Scanner(System.in);

public static void main(String[] args) {
	int opcio = 0; // Desa l'opció escollida per l'usuari

	int [] combinacio = new int [4];
	
	boolean comb = false;
	
	boolean guanyador = false;
	
	do {
		
		opcio = mostrarMenu();
		
			switch (opcio) {
		
			case 1:
				
				combinacio = Generacio.generarCombinacio(combinacio);
				
				comb = true;
				
				break;
			
			case 2: 
				
				if (comb == true) {
					
                    guanyador = Joc.jugarPartida(combinacio, guanyador);
				}
                    
               else {
                    	
                    
                   System.out.println("Has de generar una combinació abans de poder jugar una partida");
                }

            break;
			
			case 0: 
				
				System.out.println();
				System.out.println("Fi del Joc");
				
			break;
			
			default: 
				
				System.out.println("Error, introdueix un valor correcte");
				System.out.println();
		}
	}
	
	while (opcio != 0);
	
}

private static int mostrarMenu() {
	System.out.println("-----------MASTERMIND-----------");
	
	System.out.println();
	
	System.out.println("1- Generar Combinació");
	
	System.out.println("2- Jugar Partida");
	
	System.out.println("0- Sortir");
	
	System.out.println();
	
	boolean correcte = false;
	
	int opcio = 0;
	
	do {
		System.out.println("Escull una opció: ");

		try {
			
			opcio = src.nextInt();
			
			correcte = true;
		}
		
		catch (Exception e){
			
			System.out.println("Error, opció invàlida ");
			
			src.nextLine();
		}
	}
	
	while (correcte == false);
	
	return opcio;
}

}

	


package Mastermind;

import java.util.Scanner;

public class Joc {
	
	static Scanner src = new Scanner(System.in);

	public static boolean jugarPartida(int[] combinacio, boolean guanyador) {
		// TODO Auto-generated method stu								
		char [] tauler = new char [4];
		
		int [] resposta = new int [4];
		
		
		do {
			
			preguntarNumero(combinacio, resposta);
			
			tauler = analitzarResposta(combinacio, resposta);
			
			guanyador = comprovarGuanyador(combinacio, resposta, guanyador, tauler);
		
		}while (guanyador == false);
		
		if (guanyador == true) {
			
			System.out.println("FELICITATS, has guanyat!");
			System.out.println();
		}
		
	return guanyador;
	}

	private static boolean comprovarGuanyador(int[] combinacio, int[] resposta, boolean guanyador, char[] tauler) {
		// TODO Auto-generated method stub
		
		if (tauler[0] == 'O' && tauler[1] == 'O' && tauler[2] == 'O' && tauler[3] == 'O') {
			
			guanyador = true;
			
		}
		
		return guanyador;
		
	}

	private static char[] analitzarResposta(int[] combinacio, int[] resposta) {
		// TODO Auto-generated method stub
		
		char[] tauler = {'_', '_', '_', '_'};
		
		tauler = adivinarNum(combinacio, resposta, tauler);;
		
		tauler = plenaNum(combinacio, resposta, tauler);
		
		
		System.out.println("Si el simbol es: 'O' es plena, si es 'X' falta adivinar la posicio");
		
		System.out.println(combinacio[0] + " " + combinacio[1] + " " + combinacio[2] + " " + combinacio[3] + " ");
		System.out.println(tauler[0] + " " + tauler[1] + " " + tauler[2] + " " + tauler[3] + " ");
		
		return tauler;
	}

	private static char[] plenaNum(int[] combinacio, int[] resposta, char[] tauler) {
		// TODO Auto-generated method stub
		
		if(combinacio[0] == resposta[0]) {
			
			tauler[0] = 'O';
			
		}
		if(combinacio[1] == resposta[1]) {
			
			tauler[1] = 'O';
			
		}
		if(combinacio[2] == resposta[2]) {
			
			tauler[2] = 'O';
			
		}
		if(combinacio[3] == resposta[3]) {
			
			tauler[3] = 'O';
			
		}
		
		
		return tauler;
	}

	private static char[] adivinarNum(int[] combinacio, int[] resposta, char[] tauler) {
		// TODO Auto-generated method stub
		
		if(combinacio[0] == resposta[1] || combinacio[0] == resposta[2] || combinacio[0] == resposta[3]) {
			
			tauler[0] = 'X';
			
			
		}
		if(combinacio[1] == resposta[0] || combinacio[1] == resposta[2] || combinacio[1] == resposta[3]) {
			
			tauler[1] = 'X';
			
		}
		if(combinacio[2] == resposta[0] || combinacio[2] == resposta[1] || combinacio[2] == resposta[3]) {
			
			tauler[2] = 'X';
			
		}
		if(combinacio[3] == resposta[0] || combinacio[3] == resposta[2] || combinacio[3] == resposta[1]) {
			
			tauler[3] = 'X';
			
		}
		
		return tauler;
	}

	private static int[] preguntarNumero(int[] combinacio, int[] resposta) {
		// TODO Auto-generated method stub
		
		int primer;
		int segon;
		int tercer;
		int quart;
		
		System.out.println("Digues 4 numeros del 0 al 9");
		
		do {
		primer = llegirNum("primer num: ", "ERROR, introdueix un numero entre 0 i 9");
		}while(primer > 9 || primer < 0);
		
		do {
		segon = llegirNum("segon num: ", "ERROR, introdueix un numero entre 0 i 9");
		}while(segon > 9 || segon < 0);
		
		do {
		tercer = llegirNum("tercer num: ", "ERROR, introdueix un numero entre 0 i 9");
		}while(tercer > 9 || tercer < 0);
		
		do {
		quart = llegirNum("quart num: ", "ERROR, introdueix un numero entre 0 i 9");
		}while(quart > 9 || quart < 0);
		
		resposta[0] = primer;
		resposta[1] = segon;
		resposta[2] = tercer;
		resposta[3] = quart;
		
		return resposta;
		
	}

	private static int llegirNum(String missatge1, String missatge2) {
		// TODO Auto-generated method stub
		
		boolean correcte = false;
		
		int opcio = 0;
		
		do {
			System.out.println(missatge1);

			try {
				
				opcio = src.nextInt();
				
				correcte = true;
			}
			
			catch (Exception e){
				
				System.out.println(missatge2);
				
				src.nextLine();
			}
		}
		
		while (correcte == false);
		
		return opcio;
	}
		
	

	}

	

	
package Flota;

import java.util.Hashtable;
import java.util.Scanner;

public class Jugador {
	
	static Scanner src = new Scanner(System.in);
	static Hashtable<String, Integer> jugadors = new Hashtable <String, Integer>();

	public static String definirJugador() {
		// TODO Auto-generated method stub
		
		String usuari;
		System.out.println("Digues el teu nom d'usuari: ");
		usuari = src.nextLine();
		
		if (!jugadors.containsKey(usuari)) {
			
			jugadors.put(usuari, 0);
			
		}
		
		return usuari;
	}

	public static void actualitzarResultat(String jugador1, String jugador2, int resultat) {
		// TODO Auto-generated method stub
		
		if (jugadors.containsKey(jugador1) && resultat == 1) {
			
			jugadors.put(jugador1, jugadors.get(jugador1) + 1);
			
		}
		
		else if (jugadors.containsKey(jugador2) && resultat == 2) {
			
			jugadors.put(jugador2, jugadors.get(jugador2) + 1);
			
		}
		
	}

	public static void veureResultat(String jugador1, String jugador2) {
		// TODO Auto-generated method stub
		
		if (jugadors.containsKey(jugador1) && jugadors.containsKey(jugador2)) {
			
			System.out.println();
			
			System.out.println("Partides guanyades per " + jugador1 + " : " + jugadors.get(jugador1));
			
			System.out.println("Partides guanyades per " + jugador2 + " : " + jugadors.get(jugador2));
			
		}
		
	}
	

		
	}



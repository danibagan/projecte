package inicials;

import java.util.Scanner;

public class Notes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		for(int casos = src.nextInt(); casos > 0;casos--) {
			
			int f = src.nextInt();
			int c = src.nextInt();
			
			int [][] mat = new int [f][c];
			
			for(int i = 0; i < f; i++) {	//per cada fila
				
				for(int j = 0; j < c; j++) {	// per cada columna
					
					mat [i][j]= src.nextInt();	
					
				}
				
			}
			for(int i= 0; i<f;i++) {
				
				int suma = 0;
				for(int j=0;j<f;j++) {
					suma = suma + mat[i][j];
				}
				System.out.println(suma / f );
			}
			
		}
	}

}

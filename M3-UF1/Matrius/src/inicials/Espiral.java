package inicials;

import java.util.Scanner;

public class Espiral {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int mesura = src.nextInt();
		
		if (mesura % 2 == 0) {
			System.out.println("Ha de ser un numero impar");
		}
		
		while (mesura != 0) {
			
		int mat [][] = new int [mesura][mesura];
			
		
		for(int i = 0; i < mesura; i++) {	//per cada fila
			
			for(int j = 0; j < mesura; j++) {	// per cada columna
				
				mat [i][j]= src.nextInt();	
				
			}
			
		}
		
		int posX = mesura / 2;
		int posY = mesura / 2;
		
		int [][] espiral = { {-1,0}, {0,1}, {1,0}, {0,-1} };
		
		int contestrelles = 0;
		int resta = 2;
		int direccio = 0;
		int valor = 1;
		
		while(posX >= 0 && posY >= 0 && posX < mesura && posY < mesura) {
			
			contestrelles = contestrelles + mat[posY][posX];
			resta = resta - 1;
			
			if (resta == 0) {
				valor++;
				resta = valor;
				direccio = (direccio + 1) % 4;
			}
			posX = posX + espiral[direccio][0];
			posY = posY + espiral[direccio][1];
			
		}
		
		System.out.println(contestrelles);
		
		mesura = src.nextInt();
		
		}
		
	}

}

package inicials;

import java.util.Scanner;

public class EscriuMatriu1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int files = src.nextInt();
		int columnes = src.nextInt();
		
		int [][] mat = new int [files][columnes];
		
		for(int i = 0; i < files; i++) {	//per cada fila
			
			for(int j = 0; j < columnes; j++) {	// per cada columna
				
				mat [i][j]= src.nextInt();	
				
			}
			
		}
		
		// hem llegit la matriu sencera
		
		//llegim la posicio de la casella a consultar
		
		int ii = src.nextInt();
		int jj = src.nextInt();
		
		// mostrem el contingut de la matriu
		
		for (int i = 0; i < files;i++) {
			
			for(int j = 0; j < columnes; j++) {
				
				System.out.print(mat[i][j] + " ");
				
			}
			
			System.out.println();
			
		}
		
		//moastrem el valor guardat en ii, jj
		System.out.println(mat[ii][jj]);
		
	}

}

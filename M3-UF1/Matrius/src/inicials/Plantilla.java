package inicials;

import java.util.Scanner;

public class Plantilla {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		 Scanner src = new Scanner(System.in);

		 for(int casos = src.nextInt(); casos > 0; casos--) {
	        int f = src.nextInt();
	        int c = src.nextInt();

	        int[][] mat = new int[f][c];

	        for (int i = 0; i < f; i++) {
	            for (int j = 0; j < c; j++) {
	                mat[i][j] = src.nextInt();
	            }
	        }

	        for (int i = 0; i < f; i++) {
	            for (int j = 0; j < c; j++) {
	                System.out.print(mat[i][j] + " ");
	            }
	                System.out.println();
	        }

		 }
	}

}

package inicials;

import java.util.Scanner;

public class Eleccions {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		   Scanner scanner = new Scanner(System.in);

	        // Llegir la mida de la casella de vots
	        int midaCasella = scanner.nextInt();

	        // Generar i imprimir la matriu amb les marques
	        char[][] matriuResultat = generarMatriuMarca(midaCasella);
	        imprimirMatriu(matriuResultat);

	        // Tancar l'objecte Scanner per evitar fugues de memòria
	        scanner.close();
	    }

	    // Mètode per generar la matriu amb les marques
	    public static char[][] generarMatriuMarca(int midaCasella) {
	        char[][] matriu = new char[midaCasella][midaCasella];

	        // Omplir la matriu amb '.'
	        for (int i = 0; i < midaCasella; i++) {
	            for (int j = 0; j < midaCasella; j++) {
	                matriu[i][j] = '.';
	            }
	        }

	        // Marcar les vores amb 'X'
	        for (int i = 0; i < midaCasella; i++) {
	            matriu[i][0] = 'X';
	            matriu[i][midaCasella - 1] = 'X';
	            matriu[0][i] = 'X';
	            matriu[midaCasella - 1][i] = 'X';
	        }

	        // Marcar les dues diagonals amb 'X'
	        for (int i = 0; i < midaCasella; i++) {
	            matriu[i][i] = 'X';
	            matriu[i][midaCasella - 1 - i] = 'X';
	        }

	        return matriu;
	    }

	    // Mètode per imprimir la matriu
	    public static void imprimirMatriu(char[][] matriu) {
	        for (int i = 0; i < matriu.length; i++) {
	            for (int j = 0; j < matriu[i].length; j++) {
	                System.out.print(matriu[i][j]);
	            }
	            System.out.println();  // Nova línia per a cada fila
	        }
	    }
	}

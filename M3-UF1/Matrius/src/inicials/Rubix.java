package inicials;

import java.util.Scanner;

public class Rubix {

    public static void main(String[] args) {
        Scanner src = new Scanner(System.in);

        int mesura = src.nextInt();

        while (mesura != 0) {
            char[][] mat = new char[mesura][mesura];

            src.nextLine();

            for (int i = 0; i < mesura; i++) {
            	
                String f = src.nextLine();

                for (int j = 0; j < mesura; j++) {
                	
                    mat[i][j] = f.charAt(j);
                }
            }           
            	
            String rotar = src.nextLine();
            
            while (!rotar.equals("x")) {
            	
            	char fiCo = rotar.charAt(0);
            	
            	char num = rotar.charAt(1);
            	
            	if (fiCo == 'f') { //variable per files i columnes per aber quines girem
            	    if (num > 0) {
            	    	
            	        char ant = mat[num - 1][mesura - 1];

            	        for (int j = mesura - 1; j > 0; j--) {
            	        	
            	        	mat[num - 1][j] = mat[num - 1][j - 1];
            	        }

            	        mat[num - 1][0] = ant;
            	    }else {		// columnes
            	    	
            	        char ant = mat[-num - 1][0];

            	        for (int j = 0; j < mesura - 1; j++) {
            	        	
            	        	mat[-num - 1][j] = mat[-num - 1][j + 1];
            	        }

            	        mat[-num - 1][mesura - 1] = ant;
            	    }
            	}else {
            		
            		if (num > 0) {
            			
            	        char ant = mat[mesura - 1][num - 1];

            	        for (int i = mesura - 1; i > 0; i--) {
            	        	
            	        	mat[i][num - 1] = mat[i - 1][num - 1];
            	        }

            	        mat[0][num - 1] = ant;
            	    }else {
            	    	
            	        char ant = mat[0][-num - 1];

            	        for (int i = 0; i < mesura - 1; i++) {
            	        	
            	        	mat[i][-num - 1] = mat[i + 1][-num - 1];
            	        }

            	        mat[mesura - 1][-num - 1] = ant;
            	    }
            		
            	}
            	
            	rotar = src.nextLine();
	            
            }     
            
            for (int i = 0; i < mesura; i++) {
            	
                for (int j = 0; j < mesura; j++) {
                	
                    System.out.print(mat[i][j]);
                }
                
                System.out.println();
            }
            
            System.out.println("---");
            
            mesura = src.nextInt();
            
        }
    }
}

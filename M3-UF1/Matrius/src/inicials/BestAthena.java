package inicials;

import java.util.Scanner;

public class BestAthena {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
for(int casos = src.nextInt(); casos > 0;casos--) {
			
			int f = src.nextInt();
			int c = src.nextInt();
			
			int [][] mat = new int [f][c];
			
			for(int i = 0; i < f; i++) {	//per cada fila
				
				for(int j = 0; j < c; j++) {	// per cada columna
					
					mat [i][j]= src.nextInt();	
					
				}
				
			}
		
			int x = src.nextInt();
			int y = src.nextInt();
			
			boolean correcte = (mat[x][y] == 1);
			if (correcte) {
				int maxf = x + 1;
				int maxc = y + 1;
				int minf = x - 1;
				int minc = y - 1;
				if (maxf == f) maxf--;
				if (maxc == c) maxc--;
				if (minf < 0) minf ++;
				if (minc < 0) minc ++;
				int cont = 0;
				
				for (int i = minf; i <= maxf; i++)
					for (int j = minc; j <= maxc; j++) 
						if (!(i == x && j == y) && mat[i][j] == 2) cont++;
				System.out.println(cont);
			}
			else System.out.println("-1");
		}

			
		}	
	}


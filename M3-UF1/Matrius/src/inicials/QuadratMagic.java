package inicials;

import java.util.Scanner;

public class QuadratMagic {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);

		 for(int casos = src.nextInt(); casos > 0; casos--) {
	        int mesura = src.nextInt();
	       

	        int[][] mat = new int[mesura][mesura];

	        for (int i = 0; i < mesura; i++) {
	            for (int j = 0; j < mesura; j++) {
	                mat[i][j] = src.nextInt();
	            }
	        }

	        for (int i = 0; i < mesura; i++) {
	            for (int j = 0; j < mesura; j++) {
	                System.out.print(mat[i][j] + " ");
	            }
	                System.out.println();
	        }

		 }
		
	}

}

package inicials;

import java.util.Scanner;

public class Gava {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
Scanner src = new Scanner(System.in);
		
		final int N = src.nextInt();
		int felicitat = 0;
		
		int[][] mat = new int[N][3];
        
        for (int i = 0; i < N; i++) {
        	for (int j = 0; j < 3; j++) {
        		mat[i][j] = src.nextInt();
        	}
        }
        
        for (int f = 0; f < N; f++) {
        	int max = mat[f][0];
        	int posi = f;
        	int posj = 0;
        	for (int c = 1; c < 3; c++) {
        		if (mat[f][c] > max) {
        			max = mat[f][c];
        			posj = c;
        		}
        	}
        	felicitat = felicitat + mat[posi][posj];
        	if (posi + 1 < N) mat[posi+1][posj] = 0;
        }
        System.out.println(felicitat);
	}

}

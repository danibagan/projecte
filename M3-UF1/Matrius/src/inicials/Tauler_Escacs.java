package inicials;

import java.util.Scanner;

public class Tauler_Escacs {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);

	
	        int f = src.nextInt();
	        int c = src.nextInt();

	        String[][] mat = new String[f][c];

	        for (int i = 0; i < f; i++) {
	            for (int j = 0; j < c; j++) {
	                if ((i + j) % 2 == 0) {
	                    System.out.print(". ");
	                } else {
	                    System.out.print("# ");
	                }
	            }
	            System.out.println();
	        }
	    }
	}
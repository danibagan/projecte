package inicials;

import java.util.Scanner;

public class NatacioSincronitzada {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Scanner src = new Scanner(System.in);
		
		for (int casos = src.nextInt(); casos > 0; casos--) {
			int k = src.nextInt()/2;
			int n = src.nextInt();
			
			//int [][] mat1 = new int [k][k];
		//	int [][] mat2 = new int [k][k];
			int valor;
			int [] posxi = new int [n+1]; //fila on es troba inicialment cada nadadora
			int [] posyi = new int [n+1]; //columna on es troba inicialment cada nadadora
			
			int [] posxf = new int [n+1]; //fila on es troba finalment cada nadadora
			int [] posyf = new int [n+1]; //columna on es troba finalment cada nadadora
		//	int [] num = new int [n+1];
			
			
			for (int i = 0; i < k; i++) {
				for (int j = 0; j < k; j++) {
					valor = src.nextInt();
					if (valor >= 1 && valor <= n) {
						posxi[valor] = i;
						posyi[valor] = j;
					}
				}
			}
			
			for (int i = 0; i < k; i++) {
				for (int j = 0; j < k; j++) {
					valor = src.nextInt();
					if (valor >= 1 && valor <= n) {
						posxf[valor] = i;
						posyf[valor] = j;
					}
				}
			}
			
			boolean correcte = true;
			k = 2;  //per cada nadadora
			int dx = posxi[1] - posxf[1];
			int dy = posyi[1] - posyf[1];
			while (k <= n && correcte) {
				if (posxi[k] - posxf[k] != dx || posyi[k] - posyf[k] != dy) 
					correcte = false;
				k++;
			}
			
			if (correcte) {
				//if (dx < 0) dx = -dx;
				dx = Math.abs(dx);
				dy = Math.abs(dy);
				if (dx > dy)
					System.out.println(dx);
				else 
					System.out.println(dy);
			}
			else System.out.println("NO SINCRONITZADA");
				
			
			/*for (int i = 0; i < k; i++) {
				for (int j = 0; j < k; j++) {
					mat2[i][j] = src.nextInt();
					if (mat2[i][j] >= 1 && mat2[i][j] <= n) {
						posxf[mat1[i][j]] = i;
						posyf[mat1[i][j]] = j;
						
					}
				}
			}*/
		}
	}

}


package inicials;

import java.util.Scanner;

public class Bomberman {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        Scanner src = new Scanner(System.in);

        int f = src.nextInt() + 2;
        int c = src.nextInt() + 2;

        int[][] matriu = new int[f][c];

        for (int i = 0; i < f; i++) {
            for (int j = 0; j < c; j++) {
                if (i == 0 || i == f - 1 || j == 0 || j == c - 1) {
                    matriu[i][j] = 0;
                }
                else {
                    matriu[i][j] = src.nextInt();
                }
            }
        }

        int fx = src.nextInt() + 1;
        int cy = src.nextInt() + 1;

        int sum = 0;

        for (int i = 0; i < f; i++) {
            sum = sum + matriu[i][cy];

        }

        for (int j = 0; j < c; j++) {
            sum = sum + matriu[fx][j];
        }

        sum = sum - matriu[fx][cy];

        System.out.println(sum);
    }

}
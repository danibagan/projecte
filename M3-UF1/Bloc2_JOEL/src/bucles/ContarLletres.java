package bucles;

import java.util.Scanner;

public class ContarLletres {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		String frase;
		int cont;
		int pos;
		
		frase = src.nextLine();
		
		while (!frase.equals("FI")){
			cont = 0;
			pos = 0;
			while(pos < frase.length()) {
				if ((frase.charAt(pos) >= 'A' && frase.charAt(pos) <= 'Z') || (frase.charAt(pos) >= 'a' && frase.charAt(pos) <= 'z')){
					cont++;
				}
				pos++;
			}
			System.out.println(cont + " ");
			frase = src.nextLine();
		}
		System.out.println();



	
	}
}

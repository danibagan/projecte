package bucles;

import java.util.Scanner;

public class PrimerBucle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int num = src.nextInt();
		while (num != 0) {
			System.out.println((num + 1));
			num = src.nextInt();
		}
		
	}

}

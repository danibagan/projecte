package bucles;

import java.util.Scanner;

public class ControlDeNoms {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		String nom= src.next();
		int pos;
		int repeticio = 0;
		char car;
		
		pos = 1;
		car = nom.charAt(0);
		
		while (pos < nom.length()) {
			if (nom.charAt(pos) != car) {
				repeticio = 1;
			}
			pos++;
		}
		if (repeticio == 1) {
			System.out.println("SI");
		}
		else {
			System.out.println("NO");
		}
	}

}

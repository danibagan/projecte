package bucles;

import java.util.Scanner;

public class NumPositiu {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int max;
		int num;
		int cont = 0;
		
		max = src.nextInt();
		while (max > 0) {
			num = src.nextInt();
			if (num > 0)  {
				cont++;
			}
		max--;
		}
		System.out.println(cont);
		
	}

}

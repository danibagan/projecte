package bucles;

import java.util.Scanner;

public class ContarVocals {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Scanner src = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
		
		
		int casos = src.nextInt();
		char lletra;
		int conta=0, conte=0, conti=0, conto=0, contu=0;
		int pos = 0;
		src.nextLine();   //neteja el buffer d'entrada. Posar sempre darrere d'una lectura de número 
		
		 while (casos > 0) {
			
			conta=0; conte=0; conti=0; conto=0; contu=0;
			pos = 0;
			
			String input = src.nextLine();  
		
			//Llegeix un caracter de teclat
			
			while (pos < input.length())
			{
				lletra = input.charAt(pos);	
				switch (lletra) {
					case 'a':
					case 'A': conta = conta + 1;
						      break;
					case 'e':
					case 'E': conte = conte + 1;
						      break;
					case 'i':
					case 'I': conti = conti + 1;
						      break;
					case 'o':
					case 'O': conto = conto + 1;
						      break;
					case 'u':
					case 'U': contu = contu + 1;
						      break;		
				}
				pos++;
			}
	
			System.out.println("A: " + conta + " E: " + conte + " I: " + conti + " O: " + conto + " U: " + contu);
			
			casos--;
		}
	}
}


package bucles;

import java.util.Scanner;

public class ContadorNotes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		float nota;
		float mitjana;
		int total;
		int ex, not, be, suf, ins, md;
		
		nota = src.nextFloat();
		ex = not= be= suf = ins = md = 0;
		mitjana = 0;
		total = 0;
		
		while (nota != -1) {
			if (nota >= 0 && nota <= 10) {
				total++;
				mitjana = mitjana + nota;
				if (nota >= 9) {
					ex++;
				}
				else {
					if (nota >=7) {
						not++;
					}
					else {
						if (nota >= 6) {
							be++;
						}
						else {
							if (nota >=5) {
								suf++; 
							}
							else {
								if (nota > 3) {
									ins++;
								}
								else {
									md++;
								}
							}
						}
					}
				}
			}
			nota = src.nextFloat();
		}
		mitjana = mitjana / total;
		System.out.println("NOTES: " + total + " MITJANA: " + mitjana + " E: " + ex + " N: " + not + " B: " + be + 
				" S: " + suf + " I: " + ins + " MD: " + md);
	}

}

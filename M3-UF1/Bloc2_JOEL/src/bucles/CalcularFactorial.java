package bucles;

import java.util.Scanner;

public class CalcularFactorial {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int max;
		int num;
		long fact;
		
		max = src.nextInt();
		
		while (max > 0) {
			num = src.nextInt();
			fact = 1;
			while (num > 0) {
				fact = (fact * num);
				num--;
			}
			System.out.println(fact);
		max--;
		}
	}

}

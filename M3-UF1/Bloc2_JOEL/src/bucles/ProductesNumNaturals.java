package bucles;

import java.util.Scanner;

public class ProductesNumNaturals {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		int casos;
		
		int n;
		int suma = 0, prod = 1;
	
		casos = src.nextInt();
		
		while (casos > 0) {
			//tractem un cas de prova
			n = src.nextInt();
			if (n <= 0) {
				System.out.println("ELS NOMBRES NATURALS COMENCEN EN 1");
			}
			else {  //n és un nmbre natura
				suma = 0;
				prod = 1;
				while (n >= 1) {
					suma = suma + n;
					prod = prod * n;
					n--;
				}
				System.out.println("SUMA: " + suma + " PRODUCTE: " + prod);
			}
			
			//Finalitza el cas de prova
			casos--;
		}
		
	}

}



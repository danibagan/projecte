package bucles;

import java.util.Scanner;

public class JordiWhile2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int visites;
		int contS = 0;
		int total = 0;
		
		while (contS < 3) {
			visites = src.nextInt();
			if (visites == -1) {
				contS++;
			}
			else {
				total = visites + total;
			}
		}
		System.out.println(total);
	}

}

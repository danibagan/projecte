package bucles;

import java.util.Scanner;

public class Nota10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int num = src.nextInt();
		int contn = 0;
		int cont10 = 0;
		while (num != -1) {
			if ((num >= 0) && (num <= 10)) {
				contn++;
			}
			if (num == 10) {
				cont10++;
			}
			num = src.nextInt();	
		}
		System.out.println("TOTAL NOTES: " + contn + " NOTES10: " + cont10);
	}

}

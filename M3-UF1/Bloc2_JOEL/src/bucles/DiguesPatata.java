package bucles;

import java.util.Scanner;

public class DiguesPatata {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		int max; //indica el numero maxim de casos que tractarà
		String linea; //guarda cada linea que entrem
		
		max = src.nextInt();
		src.nextLine(); //Neteja el buffer d'entrada
		
		while (max > 0) {
			//comença el tractament d'un cas de prova
			linea = src.next();
			System.out.println(linea);
			//fi d'un cas de prova
			max = max - 1;
		}
	}

}

package bucles;

import java.util.Scanner;

public class SumarValors {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
		
		int suma;
		int pos;
		int n;
		int cont;

		int casos = reader.nextInt();
		
		while (casos > 0){
			suma = 0;
			cont = reader.nextInt();
			pos = 0;
			
			while (cont > 0) {
				n = reader.nextInt();
				pos++;
				if (pos % 3 == 0)
					suma = suma + n;
				cont--;
			}
			System.out.println(suma);
			
			casos--;
			
		}
	            
		
}
}

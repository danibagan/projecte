package bucles;

import java.util.Scanner;

public class MesGranMesPetit {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int num;
		int g;
		int p;
		
		
		num = src.nextInt();
		g = Integer.MAX_VALUE;
		g = num;
		p = Integer.MIN_VALUE;
		p = num;
		
		while (num != 0){
			if (num > g) {
				g = num;	
			}
			if (num < p) {
				p = num;
			}
			num = src.nextInt();
		}
		System.out.println(g + " " + p);
		
		
	}

}


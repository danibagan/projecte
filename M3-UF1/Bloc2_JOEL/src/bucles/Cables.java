package bucles;

import java.util.Scanner;

public class Cables {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		int casos = src.nextInt();
		
		int nCables;
		String cables;
		boolean possible;
		int m, h;
		int pos;
		
		while (casos > 0) {
			
			nCables = src.nextInt();
			possible = true;
			src.nextLine();
			m = 0;   //contador de connectors "macho"
			h = 0;   //contador de connectors "hembra"
			pos = 0;
			
			cables = src.nextLine();
			while (pos < cables.length()) {
				
				switch (cables.charAt(pos)) {
					case 'H': h++;
							break;
					case 'M': m++;
				}
				pos++;
			}
			if (h == m) System.out.println("POSIBLE");
			else System.out.println("IMPOSIBLE");
			
			casos--;
		}
	}
}


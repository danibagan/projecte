package bucles;

import java.util.Scanner;

public class SilenciALaSala {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		String entrada = src.next();
		int cont = 0;
		
		while (entrada != "sortida") {
			System.out.println("Sssh, únicament acaba quan posis sortida");
			entrada = src.next();
		}
	}

}

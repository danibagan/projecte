package bucles;

import java.util.Scanner;

public class ExplosioArcana2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int danyInicial = src.nextInt();
		int vida = src.nextInt();
		int danyFinal = 0;
		int danyTotal = 0;
		int numAtac = 1;
		
		while (danyTotal < vida) {
			danyFinal = danyInicial * numAtac;
			danyTotal = danyTotal + danyFinal;
			numAtac++;
		}
		System.out.println(numAtac - 1);
	}

}

package bucles;

import java.util.Scanner;

public class ParellsSenars {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		int num;
		int casos;
		int contp;
		int conts;
		int cont;
		
		casos = src.nextInt();
		
		while (casos > 0) {
			
			num = src.nextInt();
			contp = 0;
			conts = 0;
			cont = 1;
		
			while (cont <= num) {
				if (cont % 2 == 0) {
					contp = contp + cont;
				}
				else {
					conts = conts + cont;
				}
			cont++;
		}
		System.out.println("PARELLS: " + contp + " SENARS: " + conts);
		casos--;
		}
	}

}

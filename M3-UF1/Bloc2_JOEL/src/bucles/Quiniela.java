package bucles;

import java.util.Scanner;

public class Quiniela {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int casos;
		int local;
		int visitant;
		
		
		casos = src.nextInt();
	
		
		while (casos > 0) {
			local = src.nextInt();
			visitant = src.nextInt();
			if ((visitant < local) || (local > visitant))  {
				System.out.println("1");
			}
			if ((local < visitant) || (visitant > local)) {
				System.out.println("2");
			}
			if (local == visitant){
				System.out.println("X");
			}
			casos--;
		}
	}

}

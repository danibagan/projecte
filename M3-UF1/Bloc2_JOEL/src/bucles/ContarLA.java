package bucles;

import java.util.Scanner;

public class ContarLA {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner reader = new Scanner(System.in);
		
		
	
		int contador = 0;
		int pos;
		char lletra;
		
		int casos = reader.nextInt();
		reader.nextLine();
		String input;
		
		while (casos > 0) {
		            
			input = reader.nextLine();	
		    contador = 0;
		    pos = 0;
		   
		    while (pos < input.length()-1) {
		    	
		    	if ((input.charAt(pos) == 'l' || input.charAt(pos) == 'L' ) &&
		    			(input.charAt(pos+1) == 'a' || input.charAt(pos+1) == 'A' )) {
		    			
		    			contador=contador+1;
		    			}		   
		    	pos++;		  
		    }
		    
		    System.out.println(contador);
		    
		    casos--;

		   }
		}
}


package bucles;

import java.util.Scanner;

public class RadarValentina {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		int casos;
		int energia;
		int cont;
		boolean m;
		boolean b;
		boolean h;
		
		casos = src.nextInt();
		while (casos > 0) {
			
			m = false;
			b = false;
			h = false;
			cont = 5;
			
			while (cont > 0) {
				energia = src.nextInt();
				if (energia >= 10000) {
					m = true;
				}
				else  {
					if (energia >= 1000) {
						b = true;
					}
					else {
						h = true;
					}
				}
				cont--;
			}
			if (m == true) {
				System.out.println("M");
			}
			else if (b == true) {
				System.out.println("B");
			}
			else  {
				System.out.println("H");
			}
			casos--;
		}
	}

}

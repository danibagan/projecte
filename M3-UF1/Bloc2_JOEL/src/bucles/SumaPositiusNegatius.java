package bucles;

import java.util.Scanner;

public class SumaPositiusNegatius {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int num = src.nextInt();
		int cont = 0;

		while (num != 0) {
		if (num > 0) {
			cont++;
		}
		else  {
			cont--;
		}
		num = src.nextInt();
		}
		
		if (cont < 0) {
			System.out.println("NEGATIUS");
		}
		else if (cont > 0) {
			System.out.println("POSITIUS");
		}
		else {
			System.out.println("IGUALS");
		}
		
	}

}

package bucles;

import java.util.Scanner;

public class Blitzcrank {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int d1 = src.nextInt();
		int d2 = src.nextInt();
		int c = src.nextInt();
		
		while (d1 != 0 && d2 != 0 && c != 0) {
			if (d1 * d1 + d2 * d2 <= c * c) {
				System.out.println("SI");
			}
			else {
				System.out.println("NO");
			}
			d1 = src.nextInt();
			d2 = src.nextInt();
			c = src.nextInt();
		}
	}

}

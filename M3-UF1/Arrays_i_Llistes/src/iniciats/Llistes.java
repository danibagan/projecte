package iniciats;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;

public class Llistes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		//declaració variable ArrayList
		ArrayList<Integer> arrlista = new ArrayList<Integer>();
		
		//Afegir valors a l'arrayList
		arrlista.add(4);
		
		//Obtenir el nombre d'elements de la llista
		int n = arrlista.size();
		
		//ordenar 
		Collections.sort(arrlista);
		
		//Obtenir el valor de la llista a la posicio i
		int i = 4;
		int valor = arrlista.get(i);
		
		//Preguntar si un valor esta dins de la llista
		if (arrlista.contains(3)) {
			//.....
		}
		
		//Saber la posició d'un element dins la llista
		int pos = arrlista.lastIndexOf(valor);
		
		//Esborrar l'element d'una posició concreta (pos)
		arrlista.remove(pos);
		
		//Esborrar l'element d'un valor concret
		arrlista.remove((Integer)valor);
		
		//Elimina tots els elements de la llista
		arrlista.clear();
		
		//Saber si la llista esta buida
		if (arrlista.isEmpty()) {
			//....
		}
		
		//Modificar un valor que hi  ha dins de l'arraylist
		arrlista.set(pos, valor);
		
		//Copiar un arraylist sobre un altre
		ArrayList<Integer> arraylistcopia = (ArrayList<Integer>) arrlista.clone();
		
		//Passar l'arraylist a un array
		Object[] array = arrlista.toArray();
			}

}

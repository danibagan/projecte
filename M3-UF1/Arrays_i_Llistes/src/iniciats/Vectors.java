package iniciats;

import java.util.Scanner;

public class Vectors {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Scanner src = new Scanner(System.in);
		
		final int N = 8;  // creem una constant que sempre tindra el mateix valor
		
		int [] vector; 		 // vector que nomes contendrà numeros enters
		vector = new int[8]; //numero de caselles que tindrà el vector (grandaria)
		vector [4] = 3;      // valor que li donem a una casella del vector, en aquest cas, la casella 4 tindrà valor 3
		int pos;			 	// introduim la posició per poder recorrer caselles pel vector
		pos = 0; 
		
		while ( pos < N) {											// mentres posició sigui mes petit que el num maxim de caselles
			System.out.println("valor per la posició " + pos);
			vector [pos] = src.nextInt();
			pos++;
		}
		pos = 0; //tornem a inicialitzar la pos eprque sino tindra el valor amb el que ha acabt el bucle
		
		//mostrar el contingut del vector
		while (pos < N) {
			System.out.println(vector[pos] + " ");
			pos++;
		}
		
		
		
		
		
		
	}

}
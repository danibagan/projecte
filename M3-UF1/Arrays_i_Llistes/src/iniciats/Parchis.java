package iniciats;

import java.util.Scanner;

public class Parchis {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		sc.nextLine();
		
		for (;casos > 0;casos--) {
			
			String [] caselles = sc.nextLine().split(" ");
			int inicial = Integer.parseInt(caselles[0]);
			
			for (int j = 1; j < caselles.length; j++) {
				int tirada = Integer.parseInt(caselles[j]);
				if(inicial != 8) {
					if(inicial + tirada > 8 ) {
						inicial = 8 - ((inicial + tirada)  -8 );
					} else {
						inicial = inicial + tirada;
					}
				}
			}
			System.out.println(inicial);			
		}
		sc.close();
	}
}

	

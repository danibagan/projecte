package iniciats;

import java.util.Scanner;

public class Index_de {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		for ( int casos = src.nextInt(); casos > 0; casos--) {
			int n = src.nextInt();
			int [] vector = new int [n];
			
			for (int pos = 0; pos < n; pos++) {
				vector[pos] = src.nextInt();
			}
			int element = src.nextInt();
			int i = 0;
			boolean trobat = false;
			while (i<n && trobat == false) {
				if (vector[i] == element) {
					trobat = true;
				}
				else {
					i++;
				}
			}
			if (trobat == true) {
				System.out.println(i);
			}
			else {
				System.out.println("-1");
			}
		}
		}
}

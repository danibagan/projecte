package iniciats;

import java.util.Scanner;

public class CambialoUnPoco {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		for(int casos = src.nextInt(); casos>0; casos--) {
			int n = src.nextInt();
			int [] vector = new int [n];
			
			for (int pos =0; pos < n; pos++) {
				vector[pos]= src.nextInt();
			}
			int v1= src.nextInt();
			int v2 = src.nextInt();
			
			int pos = 0;
			
			while (pos < n) {
				if (vector[pos]==v1) {
					vector [pos] = v2;
				}
				pos++;
			}
			pos = 0;
			while (pos < n) {
				System.out.print(vector[pos] + " ");
				pos++;
			}
			System.out.println();
		}
	}

}

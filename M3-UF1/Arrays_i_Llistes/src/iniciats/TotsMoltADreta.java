package iniciats;

import java.util.Scanner;

public class TotsMoltADreta {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
int casos = src.nextInt();
		
		for (; casos > 0; casos--) {
			
			int max = src.nextInt();
			
			int i;
			
			int [] numeros = new int[max];
			
			//Omplim el vector de valors
			for (i = 0; i < max; i++)
				
				numeros[i] = src.nextInt();
			
			int desp = src.nextInt();
			
			for (; desp > 0; desp--) {
				
				int ultim = numeros[max-1]; 
				//Ens guardem l�ltim valor de l'array
				for (i = max - 1; i > 0; i--)
					
					numeros[i] = numeros[i-1];
				
				numeros[0] = ultim;
			}	
			//Mostrem el vector modificat
			for (i = 0; i < max - 1; i++)
				
				System.out.print(numeros[i] + " ");
			
			System.out.println(numeros[max-1]);
			
		}

	}
}


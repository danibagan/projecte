package iniciats;

import java.util.ArrayList;
import java.util.Scanner;

public class DosPatitos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Scanner src = new Scanner(System.in);
		
		ArrayList<Integer> nums = new ArrayList<Integer>();
		ArrayList<Integer> cantats = new ArrayList<Integer>();
		
		for (int i = 0; i < 9; i++) {
			nums.add(src.nextInt());
		}
		
		for (int i = 0; i < 4; i++) {
			int valor = src.nextInt();
			if (nums.contains(valor)) {
				int pos = nums.indexOf(valor);
				nums.set(pos, -1);
				cantats.add(valor);
			}
		}
		
		//mostrem el resultat
		
		for (int i = 0; i < nums.size(); i++) {
			
			if (i % 3 == 0 && i != 0) System.out.println();
			
			if (nums.get(i) == -1)
					System.out.print("* ");
			else
					System.out.print(nums.get(i) + " ");
				
				
		}
		System.out.println();
		System.out.println(cantats);
	}
}

	
package iniciats;

import java.util.Scanner;

public class DNI {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Scanner sc = new Scanner(System.in);
		
		int casos =sc.nextInt();
		sc.nextLine();
		
		String letras= "TRWAGMYFPDXBNJZSQVHLCKE";
		//char [] vec = {'T', 'R', 'W', 'A', 'G' ,'M', 'Y','F','P','D',
		//		'X','B', 'N','J','Z','S','Q','V','H','L','C','K','E'};
		
		for(int i=0;i<casos;i++){
			
			boolean valido=true;
			String dni = sc.nextLine();
			
			char lletra = dni.charAt(dni.length()-1);
			/* Alttra manera
			dni = dni.substring(0, dni.length()-1);
			numero = Integer.parseInt(dni);
			*/
			//Convertim els 8 n�meros del dni, que s�n car�cters en un �nic n�mero per poder aplicar el m�dul
			long numero = 0;
			for (int j = 0; j < 8; j++) {
			
				numero = numero * 10 + (dni.charAt(j) - '0');
			}
			
					
			//Fem el % 23 i comprovem la lletra corresponent a l'String de lletres
			int pos = (int)(numero%23);
			
			if(letras.charAt(pos) == lletra) {
			//if(vec[pos] == lletra) {
				System.out.println("valid");
			}
			else {
				System.out.println("invalid");
			}		
			
		}
	}
}


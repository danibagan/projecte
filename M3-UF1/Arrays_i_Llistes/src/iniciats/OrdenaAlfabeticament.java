package iniciats;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class OrdenaAlfabeticament {

	public static void main(String[] args) {
// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);

		ArrayList lista = new ArrayList();

		int casos = src.nextInt();
		src.nextLine();

		while (casos > 0) {

			lista.clear();
			String entrada = src.nextLine();
			int pos = 0;

			while (pos < entrada.length()) {

				char letra = entrada.charAt(pos);
				lista.add(letra);
				pos++;
			}

			Collections.sort(lista);

			pos = 0;
			while (pos < entrada.length()) {
				if (!lista.get(pos).equals(" ")) {
					System.out.print(lista.get(pos));
				}
				pos++;
			}
			casos--;
		}

	}
}

package iniciats;

import java.util.ArrayList;
import java.util.Scanner;

public class Tots_A_La_Dreta {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 Scanner src = new Scanner(System.in);

	        int casos = src.nextInt();
	        src.nextLine();

	        int mesura = casos - 1;

	        ArrayList<Integer> lista = new ArrayList<>();

	        while (casos > 0) {

	            int num = src.nextInt();

	            lista.add(num);

	            casos--;

	        }

	        int ultim = lista.get(mesura);

	        lista.remove(mesura);

	        System.out.print(ultim + " ");

	        int pos = 0;

	        while (pos < mesura) {
	            int num = lista.get(pos);
	            System.out.print(num + " ");
	            pos++;
	        }

	    }
	}

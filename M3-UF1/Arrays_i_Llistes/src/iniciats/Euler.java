package iniciats;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Euler {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		 ArrayList<Integer> arrlista = new ArrayList<Integer>();
		
		for(int casos = src.nextInt(); casos > 0; casos--) {
			
			int k = src.nextInt();
			
			for(int i = 0; i < k; i++) {
				
				arrlista.add(src.nextInt());
				
			}
			
			Collections.sort(arrlista);
			
			while(arrlista.size() > 0) {
				
				int suma = arrlista.get(0) + arrlista.get(arrlista.size() - 1);
				
				System.out.println(suma + " ");
				
				arrlista.remove(arrlista.size()- 1);
				arrlista.remove(0);
				
			}
			
		}
	}

}

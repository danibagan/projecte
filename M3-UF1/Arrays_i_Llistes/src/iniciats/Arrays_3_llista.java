package iniciats;

import java.util.Scanner;
import java.util.ArrayList;

public class Arrays_3_llista {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		ArrayList<Integer> lista = new ArrayList<Integer>(); //Creem llista
			
		int valor;
		
		valor = src.nextInt();
		do {
			lista.add(valor);
			valor = src.nextInt();
		} while (valor != -1);
		
		int pos = src.nextInt(); //Demanem la posició
		
		//Mostrem per pantalla el contingut de l'arraylist
		System.out.print("[");
		for (int i = 0; i < lista.size() -1; i++)
			System.out.print(lista.get(i) + ", ");
		
		//L'ultim valor cal escriure'l fora, perque es diferent a la resta
		System.out.println(lista.get(lista.size()-1) + "]");
		System.out.print(lista.get(pos));
	}

}

package iniciats;

import java.util.ArrayList;
import java.util.Scanner;

public class Bloodborn_Strings {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int casos = src.nextInt();
		src.nextLine();

		while(casos > 0) {

		String paraula = src.nextLine();
		
		int pos=0;
		
		boolean repetit = false;
		
		while(pos < paraula.length()-1 && repetit == false) {
			
			if (paraula.charAt(pos)==paraula.charAt(pos+1)) {
				
				repetit = true;
				
			}
				
				pos++;
			
		}
		
		if (repetit == true) {
			
			System.out.println("SI");
			
		}
		
		else {
			System.out.println("NO");
		}
		casos--;
	

		}
		
	}	

}
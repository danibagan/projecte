package iniciats;

import java.util.Scanner;

public class Arrays_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		final int K = src.nextInt(); // Constant que te la grandaria del vector
		int [] vect;    			 // declara una variable de tipus vector de enters. El vector encara no esta creat.
		vect = new int [K];			 // creem un vector buit de K posicions
		// omplim el vector amb els nombres que llegim d'entrada
		int i; //index del vector
		
		i = 0;
		while (i < K) {
			vect [i] = src.nextInt();
			i++;
		}
		// ara el valor d' i es K
		
		//llegim la posició del vector que volem mostrar
		int pos = src.nextInt();
		
		//mostrem el contingut del vector
		i = 0;
		while (i < K) {
			System.out.print(vect[i] + " ");
			i++;
		}
		//mostrar el contingut de la posició pos
		System.out.print("\n" + vect[pos]);
	}

}

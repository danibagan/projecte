package iniciats;

import java.util.Scanner;

public class MultiplicaTot {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int casos = src.nextInt();
		
		while ( casos > 0) {
			final int K = src.nextInt();
			int [] vector;
			vector = new int [K];
			int i;
			i = 0;
			while(i < K) {
				vector[i] = src.nextInt();
				i++;
			}
			i = 0;
			int num = src.nextInt();
			while (i < K) {
				
				System.out.print(num * vector[i] + " ");
				i++;
			}
			casos--;
		}
	}

}

package senseif;

import java.util.Scanner;

public class QuadratBenFormat {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		double aresta;
		aresta = src.nextDouble();
		
		double area = aresta * aresta;
		
		System.out.printf("%015.3f\n", area);
	}

}

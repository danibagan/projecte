package senseif;

import java.util.Scanner;

public class RestaurarTodo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int status = src.nextInt();
		int hp = src.nextInt();
		int max_hp = src.nextInt();
		
		if (hp == 0)   {
			System.out.println("DEBILITAT");
		}
		else if ((hp < max_hp) && (status == 0)) {
			System.out.println("CURAR");
		}
		else if ((hp == max_hp) && (status == 0)) {
			System.out.println("RES");
		}
		else if ((status == 1) && (hp != 0) && (hp < max_hp)) {
			System.out.println("CURAR I ESTAT");
		}
		else if ((hp == max_hp) && (status == 1)) {
			System.out.println("ESTAT");
		}
	}

}
